/*
 * Author: Brandon Quinn
 * Copyright (c) 2014 - Brandon Quinn
 */

package moneymanager.main;

import javax.swing.UIManager;
import moneymanager.gui.MainWindow;

public class MoneyManager {
	
	static IncrementManager manager = new IncrementManager();
	
	public static void main(String args[]) {
		
		try {
			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		MainWindow mainInterface = new MainWindow();
		mainInterface.setVisible(true);
	}
}