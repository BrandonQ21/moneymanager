/*
 * Author: Brandon Quinn
 * Copyright (c) 2014 - Brandon Quinn
 */

package moneymanager.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class IncrementManager {

	static File balanceFile;
	static File previousIncrementFile;
	
	public static double WEEKLY_SPENDING_ALLOWANCE = 50.0;
	
	public IncrementManager() {
		
		balanceFile 			= new File("balance");
		previousIncrementFile 	= new File("previousIncrement");
	}

	public static void addDollars(double amount) {
		
		try {
			
			Scanner scn 	= new Scanner(balanceFile);
			double balance 	= scn.nextDouble();
			balance			+= amount;
			scn.close();
			
			balance = Math.round(balance * 100.0) / 100.0;
			
			PrintWriter writer = new PrintWriter(balanceFile);
			writer.print(balance);
			writer.close();
			
		} catch(Exception e) {
			
		}
	}
	
	public static void removeDollars(double amount) {
		
		try {
			
			Scanner scn 	= new Scanner(balanceFile);
			double balance 	= scn.nextDouble();
			balance 		-= amount;
			scn.close();
			
			balance = Math.round(balance * 100.0) / 100.0;
			
			PrintWriter writer = new PrintWriter(balanceFile);
			writer.print(balance);
			writer.close();
			
		} catch(IOException e) {
			
			e.printStackTrace();
			
		}	
	}
	
	/*
	 * Simply reads the "balance" file and returns the contents.
	 */
	public static double readBalance() {
		
		double balance = 0.0;
		
		Scanner balanceScanner;
		
		try {
			
			balanceScanner 	= new Scanner(balanceFile);
			balance 		= balanceScanner.nextDouble();
			balanceScanner.close();
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
			
		}

		return balance;
		
	}
	
	/* 
	 * Using data serialization, the date presented in the arguments 
	 * will be written to a file named "previousIncrement"
	 */
	public void writeDate(Calendar calendar) {
		
		try {
			
			FileOutputStream fis 	= new FileOutputStream(previousIncrementFile);
			ObjectOutputStream ois 	= new ObjectOutputStream(fis);
			
			ois.writeObject((GregorianCalendar)calendar);
			
			ois.close();
			fis.close();
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} 
	}
	
	/* 
	 * The serialized Calendar Object stored in the "previousIncrement"
	 * file will be read and returned by this function in the form of a 
	 * Calendar Object.
	 */
	public Calendar readDate() {
		
		Calendar calendar = null;
		
		try {
			
			FileInputStream fis 	= new FileInputStream(previousIncrementFile);
			ObjectInputStream ois 	= new ObjectInputStream(fis);
			
			calendar = (Calendar)ois.readObject();
			
			ois.close();
			fis.close();
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
			
		} 
		
		return calendar;
	}
}
