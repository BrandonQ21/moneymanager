/*
 * Author: Brandon Quinn
 * Copyright (c) 2014 - Brandon Quinn
 */

package moneymanager.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import moneymanager.main.IncrementManager;

public class AddSpendingsWindow extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	protected JLabel 		enterLbl;
	protected JTextField 	spendingsInput;
	protected JButton 		confirmSpendingsBtn;
	protected JFrame 		mainFrame;
	
	public AddSpendingsWindow(JFrame mainFrame) {
		
		setLayout(new FlowLayout());
		this.mainFrame = mainFrame;
		
		setLocation					(mainFrame.getLocation());
		setTitle					("Add Spendings");
		setDefaultCloseOperation	(DISPOSE_ON_CLOSE);
		setSize						(210, 85);
		setResizable				(false);
		
		initComponents();
	}
	
	private void initComponents() {
		
		enterLbl 				= new JLabel("Enter spendings: $");
		spendingsInput 			= new JTextField();
		confirmSpendingsBtn 	= new JButton("Confirm Spendings");
		
		spendingsInput.setColumns(10);
		confirmSpendingsBtn.addActionListener(this);
		
		add(enterLbl);
		add(spendingsInput);
		add(confirmSpendingsBtn);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == confirmSpendingsBtn) {
			
			IncrementManager.removeDollars(Double.valueOf(spendingsInput.getText()));
			
			// Simply create a new window rather than update the graphics
			// saves a bit of hassle updating the GUI
			MainWindow mw = new MainWindow();
			
			mw.setLocation(mainFrame.getLocation());
			mw.setVisible(true);
			
			mainFrame	.setVisible(false);
			this		.setVisible(false);
			
		}
	}
}
