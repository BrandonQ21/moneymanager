/*
 * Author: Brandon Quinn
 * Copyright (c) 2014 - Brandon Quinn
 */

package moneymanager.gui;

import java.awt.event.ActionEvent;

import javax.swing.JFrame;

import moneymanager.main.IncrementManager;

public class AddDollarsWindow extends AddSpendingsWindow {
	private static final long serialVersionUID = 1L;

	public AddDollarsWindow(JFrame mainFrame) {
		
		super(mainFrame);
		
		mainFrame			.setTitle("Add $.");
		enterLbl			.setText("Enter $ to add:");
		confirmSpendingsBtn	.setText("Confirm Addition");
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == confirmSpendingsBtn) {
			
			IncrementManager.addDollars(Double.valueOf(spendingsInput.getText()));
			
			// Simply create a new window rather than update the graphics
			// saves a bit of hassle updating the GUI
			MainWindow mw = new MainWindow();
			mw.setLocation	(mainFrame.getLocation());
			mw.setVisible	(true);
			
			mainFrame	.setVisible(false);
			this		.setVisible	(false);
			
		}
	}	
}