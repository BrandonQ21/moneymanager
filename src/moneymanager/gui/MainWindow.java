/*
 * Author: Brandon Quinn
 * Copyright (c) 2014 - Brandon Quinn
 */

package moneymanager.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import moneymanager.main.IncrementManager;

public class MainWindow extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private JLabel 	titleLbl;
	private JLabel 	balanceLbl;
	private JButton addSpendingsBtn;
	private JButton addDollarsBtn;
	
	public MainWindow() {
		
		setLayout				(new FlowLayout(5, 5, 5));
		setTitle				("Weekly Allowances");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable			(false);
		setSize					(200, 100);
		
		initComponents();
		initLayout();
	}
	
	/*
	 * Simple assigns new objects to the declared members at the
	 * global scope of this class and changes their properties.
	 */
	private void initComponents() {
		
		titleLbl 		= new JLabel("Weekly Spendings Monitor");
		balanceLbl 		= new JLabel("Current Available Balance: $" + IncrementManager.readBalance());
		balanceLbl		.setForeground(new Color(10, 180, 10));
		addSpendingsBtn = new JButton("Add Spendings");

		addDollarsBtn 	= new JButton("Add $");
		
		addSpendingsBtn	.addActionListener(this);
		addDollarsBtn	.addActionListener(this);
	}
	
	/*
	 * Adds the various components to the frame.
	 */
	private void initLayout() {
	
		add(titleLbl);
		add(balanceLbl);
		add(addSpendingsBtn);
		add(addDollarsBtn);
	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == addSpendingsBtn) {
			
			new AddSpendingsWindow(this).setVisible(true);
			
		} else if(e.getSource() == addDollarsBtn) {
			
			new AddDollarsWindow(this).setVisible(true);
			
		}
	}
}
